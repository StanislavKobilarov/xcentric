function animationPreventOnLoad() {
  document.querySelectorAll('.element--hidden').forEach(element => element.classList.add('element--shown'));
}

document.onload = setTimeout(animationPreventOnLoad, 250);

document.querySelectorAll('.js-counter').forEach(
  counter => new countUp.CountUp(counter.id, counter.dataset.number, { separator: '.' }).start()
);

Highcharts.chart('dashboard-bar', {
  colors: ['#8365D7', '#56B5D2'],
  chart: {
      type: 'column',
      style: {
        fontFamily: "'DM Sans', sans-serif"
      }
  },
  title: {
      text: 'Visitor',
      align: 'left',
      margin: 28,
  },
  legend: {
      align: 'right',
      verticalAlign: 'top',
      x: 0,
      y: 25,
      reversed: true,
      floating: true,
      itemDistance: 20,
      symbolHeight: 8,
      symbolWidth: 8,
      symbolPadding: 8,
      itemStyle: {
        color: '#98A4B5',
        fontSize: '14px',
        fontWeight: '400'
      },
  },
  xAxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      crosshair: true,
      lineColor: '#FFFFFF',
      labels: {
          style: {
            color: '#98A4B5',
            fontSize: '14px',
          }
      },

  },
  yAxis: {
      min: 0,
      max: 240,
      gridLineColor: '#DDDDDD',
      gridLineDashStyle: 'longdash',
      lineColor: '#FFFFFF',
      showLastLabel: false,
      tickAmount: 6,
      title: {
        text: null,
      },
      labels: {
          style: {
            color: '#98A4B5',
            fontSize: '14px',
          }
      },
  },
  tooltip: {
    backgroundColor: '#252837',
    pointFormat: '{series.name}: <b>{point.y}G</b>',
    padding: 12,
    style: {
      color: '#F4F7F9',
    }
  },
  plotOptions: {
      column: {
          pointPadding: 0.2,
          borderWidth: 0
      }
  },
  series: [
      {
          name: 'Last Month',
          data: [100, 70, 120, 100, 50, 180, 120, 100, 140, 145, 150, 100]
      },
      {
          name: 'This Month',
          data: [125, 130, 110, 125, 75, 150, 120, 140, 110, 190, 100, 100]
      }
  ]
});

Highcharts.chart('dashboard-donut', {
  colors: ['#DDDDDD', '#8365D7', '#56B5D2'],
  chart: {
      type: 'pie',
      style: {
        fontFamily: "'DM Sans', sans-serif"
      }
  },
  accessibility: {
      point: {
          valueSuffix: 'G'
      }
  },
  title: {
      text: 'Memory',
      align: 'left',
  },
  tooltip: {
    backgroundColor: '#252837',
    pointFormat: '{series.name}: <b>{point.y}G</b>',
    padding: 12,
    style: {
      color: '#F4F7F9',
    }
  },
  plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: false
          },
          showInLegend: true
      }
  },
  series: [{
      name: 'Memory',
      colorByPoint: true,
      innerSize: '75%',
      data: [{
          name: 'Free',
          y: 5,
          legendIndex: 2
      }, {
          name: 'System',
          y: 1,
          legendIndex: 0
      }, {
          name: 'Used',
          y: 4,
          legendIndex: 1
      }]
  }],
  legend: {
    labelFormat: '{name}<br><b style="color: #333;">{y}G</b>',
    itemStyle: {
      color: '#98A4B5',
      fontSize: '14px',
      fontWeight: '400'
    },
    itemDistance: 30,
    symbolHeight: 8,
    symbolWidth: 8,
    symbolPadding: 8
  }
});

Highcharts.chart('dashboard-line', {
  chart: {
      type: 'spline',
      style: {
        fontFamily: "'DM Sans', sans-serif"
      }
  },
  xAxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      lineColor: '#FFFFFF',
      gridLineColor: '#DDDDDD',
      gridLineDashStyle: 'longdash',
      gridLineWidth: 1,
      tickmarkPlacement: 'on',
      labels: {
          style: {
            color: '#98A4B5',
            fontSize: '14px',
          }
      },
  },
  title: {
      text: 'Statistic',
      align: 'left',
      margin: 50
  },
  yAxis: {
      gridLineColor: 'transparent',
      title: {
        text: null,
      },
      labels: {
          style: {
            color: '#98A4B5',
            fontSize: '14px',
          }
      },
  },
  tooltip: {
    backgroundColor: '#252837',
    pointFormat: '<b>{point.y}</b>',
    padding: 12,
    style: {
      color: '#F4F7F9',
    }
  },
  legend: {
      align: 'right',
      verticalAlign: 'top',
      x: 0,
      y: 25,
      reversed: true,
      floating: true,
      itemDistance: 20,
      symbolHeight: 8,
      symbolWidth: 8,
      symbolRadius: 8,
      symbolPadding: 8,
      itemStyle: {
        color: '#98A4B5',
        fontSize: '14px',
        fontWeight: '400'
      },
  },
  plotOptions: {
      series: {
          fillColor: {
              linearGradient: [0, 0, 0, 300],
              stops: [
                  [0, Highcharts.getOptions().colors[0]],
                  [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
              ]
          },
          marker: {
              enabled: false,
          }
      }
  },
  series: [{
      name: 'This Month',
      data: [55, 70, 110, 50, 20, 85, 118, 95, 85, 165, 150, 130],
      lineWidth: 2,
  }, {
      name: 'Last Month',
      data: [25, 40, 70, 60, 50, 75, 101, 60, 70, 140, 130, 120],
      lineWidth: 2,
  }]
});

Highcharts.setOptions({
  colors: ['#BFE8FE']
});

Highcharts.chart('dashboard-areaspline', {
  chart: {
      type: 'areaspline',
      style: {
        fontFamily: "'DM Sans', sans-serif"
      }
  },
  xAxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
      labels: {
        enabled: false,
      },
      visible: false,
  },
  title: {
      text: 'Server Status',
      align: 'left',
  },
  yAxis: {
      gridLineColor: 'transparent',
      title: {
        text: null,
      },
      labels: {
        enabled: false,
      },
  },
  tooltip: {
    backgroundColor: '#252837',
    headerFormat: '',
    pointFormat: '<b>{point.y}G</b>',
    padding: 12,
    style: {
      color: '#F4F7F9',
    }
  },
  legend: {
    enabled: false,
  },
  plotOptions: {
      series: {
          fillColor: {
              linearGradient: [0, 0, 0, 300],
              stops: [
                  [0, Highcharts.getOptions().colors[0]],
                  [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
              ]
          },
          marker: {
              enabled: false,
          },
          lineWidth: 2,
          lineColor: '#56B5D2',
      }
  },
  series: [{
      data: [20, 40, 10, 45, 35, 80, 50]
  }]
});

const swiper = new Swiper('.swiper', {
  slidesPerView: 1,
  spaceBetween: 10,
  grabCursor: true,
  mousewheel: true,
  breakpoints: {
    768: {
      slidesPerView: 2,
      spaceBetween: 22,
    },
    1200: {
      slidesPerView: 3,
      spaceBetween: 28,
      scrollbar: {
        hide: false,
      },
    },
  },
  scrollbar: {
    el: '.swiper-scrollbar',
    draggable: true,
    hide: true,
  },
});

document.querySelectorAll('.js-toggler').forEach((toggler, i) => {
  toggler.addEventListener('click', () => {
    document.querySelectorAll('.js-element').forEach((element, index) => {
      index === i ? element.classList.toggle('shown') : element.classList.remove('shown');
    });
    document.querySelectorAll('.js-toggler').forEach((element, index) => {
      index === i ? element.classList.toggle('active') : element.classList.remove('active');
    });
  });
});

document.querySelectorAll('.js-coming-soon').forEach(comingSoon => {
  let timeout;
  comingSoon.addEventListener('click', () => {
    clearTimeout(timeout);
    document.querySelector('.toast').classList.add('toast--show');
    timeout = setTimeout(() => document.querySelector('.toast').classList.remove('toast--show'), 3000);
  });
});

document.querySelector(".header__hamburger").addEventListener("click", () => {
  document.querySelector(".hamburger-menu").classList.toggle("hamburger-menu--open");
  document.querySelector(".navigation").classList.toggle("navigation--open");
});

const modal = document.querySelector('.js-modal');

document.querySelector('.btn--close').addEventListener('click', closeModal);
document.querySelectorAll('.js-dialog-open').forEach(btn => btn.addEventListener('click', openModal));

function openModal() {
  const { target } = this.dataset;
  if (target) {
    modal.showModal();
    modal.querySelector(`.${target}`)?.classList.add('active');
  }
}

function closeModal() {
  const handler = () => {
    modal.removeEventListener('animationend', handler);
    modal.classList.remove('hidden');
    modal.close();
    modal.querySelector('.active').classList.remove('active');
  };

  modal.addEventListener('animationend', handler);
  modal.classList.add('hidden');
}

